﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.IO;
using System.Reflection;
using System.Timers;

namespace CoolParking
{
    class Program
    {
        static TimeServer timeServer = new TimeServer();
        static void Main(string[] args)
        {

            string _logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
            TimeServer logtimer = new TimeServer();
            TimeServer Withdrow = new TimeServer();
            LogService logService = new LogService(_logFilePath);

            Setting.Capacity = 10;
            Setting.FineRates = 2;
            Setting.StartingBalance = 0;
            Setting.PeriodOfWriteOff = 5;
            Setting.PeriodWriteInLog = 60;
            //Setting.Tarriff.Add(VehicleType.Truck, 2);
            //Setting.Tarriff.Add(VehicleType.PassengerCar, 5);
            //Setting.Tarriff.Add(VehicleType.Motorcycle, 1);
            //Setting.Tarriff.Add(VehicleType.Bus, Convert.ToDecimal(3.5));
            ParkingService parkingService = new ParkingService( logtimer, Withdrow,logService);
            Console.WriteLine("Write /y/ to add a car");
            var str = Console.ReadLine();
            if (str == "y")
            {

                parkingService.AddVehicle(
                        new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(),VehicleType.Bus,102  )
                    );
                parkingService.AddVehicle(
                     new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.Motorcycle, 84)
                 );
                parkingService.AddVehicle(
                     new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.PassengerCar, 85)
                 );
                parkingService.AddVehicle(
                     new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.Truck, 44)
                 );
                parkingService.AddVehicle(
                      new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.Bus, 105)
                  );
                parkingService.AddVehicle(
                     new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.Motorcycle, 77)
                 );
                parkingService.AddVehicle(
                     new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.PassengerCar, 45)
                 );
                parkingService.AddVehicle(
                     new Vehicle(Vehicle.GenerateRandomRegistrationPlateNumber(), VehicleType.Truck, 68)
                 );
            }
            parkingService.TimerLog();
            parkingService.TimerWithDrow();
            parkingService.GetLastParkingTransactions();
        }
       
        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
           

        }


    }
}
