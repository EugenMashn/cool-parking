﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public static class Setting
    { 
        public static int StartingBalance { get; set; }
        public static int Capacity { get; set; }
        public static int PeriodOfWriteOff { get; set; }
        public static int PeriodWriteInLog { get; set; }
        public static Dictionary<VehicleType,decimal> Tarriff { get; set; }
        public static decimal FineRates { get; set; }
    }
}