﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System.Collections.ObjectModel;

namespace CoolParking.BL.Models
{
    public static class Parking
    {
        public static decimal Balance { get; set; }
        public static int Capacity { get; set; }
        public static int FreeCapacity { get; set; }
        public static Collection<TransactionInfo> LastTransactionInfo { get; set; }
        public static Collection<Vehicle> Vehicles { get; set; }
    }
}