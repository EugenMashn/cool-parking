﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public readonly string Id;
        public readonly VehicleType VehicleType;
        public decimal Balance { get; internal set; }
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Regex regex = new Regex(@"\D{2}-\d{4}-\D{2}");
            if (regex.IsMatch(id) && balance >= decimal.MinValue && balance <= decimal.MaxValue)
            {
                Id = id;
                VehicleType = vehicleType;
                Balance = balance;
            }

        }
        private static Random random = new Random();

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            var numbers = "0123456789";
            string StartStr = new string(Enumerable.Repeat(chars, 4)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            string Number = new string(Enumerable.Repeat(numbers, 2)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            string EndString = new string(Enumerable.Repeat(numbers, 2)
                .Select(s => s[random.Next(s.Length)]).ToArray());
            string PlateNumber = StartStr + "-" + Number + "-" + EndString;

            return PlateNumber;

        }
    }
}