﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        readonly TimeServer _logTimer;
        readonly TimeServer _logTimerWithdrow;
        readonly LogService _logService;
        public ParkingService(TimeServer timeServer, TimeServer timeServerWithDraw , LogService logService)
        {
            _logService = logService;
            _logTimer = timeServer;
            _logTimerWithdrow = timeServerWithDraw;
        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        public int GetCapacity()
        {
            return Parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return Parking.FreeCapacity;
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.Vehicles);
        }

        public void AddVehicle(Vehicle vehicle)
        {
            Parking.Vehicles.Add(vehicle);
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vechicles = Parking.Vehicles.First(i => i.Id == vehicleId);
            if (vechicles != null)
            {
                Parking.Vehicles.Remove(vechicles);
            }
            else
            {
                throw new InvalidOperationException();
            }

        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            var vehicles = Parking.Vehicles.First(i => i.Id == vehicleId);
            if (vehicleId != null)
            {
                vehicles.Balance = vehicles.Balance + sum;
            }
        }

        public void WithDrow (object source, ElapsedEventArgs e )
        {
            foreach (var vehicle in Parking.Vehicles)
            {
                Parking.Balance += Setting.Tarriff[vehicle.VehicleType];
                vehicle.Balance -= Setting.Tarriff[vehicle.VehicleType];
                Parking.LastTransactionInfo
                    .Add(
                        new TransactionInfo
                        {
                            DateOfTransaction = DateTime.Now,
                            VehicleId = vehicle.Id,
                            Sum = Setting.Tarriff[vehicle.VehicleType]
                        }
                    );
            }
        }
        
        public void TimerWithDrow( )
        {
            _logTimerWithdrow.Interval = 1000;
            _logTimerWithdrow.Elapsed += WithDrow;
            _logTimerWithdrow.Start();
        }

        public void LogWrite( object source, ElapsedEventArgs e )
        {
            string logString = "";
            foreach(var TransactionInfo in Parking.LastTransactionInfo)
            {
                logString = TransactionInfo.VehicleId + " "+ TransactionInfo.DateOfTransaction.ToString() + " " + TransactionInfo.Sum.ToString();
                _logService.Write(logString);
                logString = "";
            }
           
        }
        public void TimerLog()
        {
            _logTimer.Interval = 1000;
            _logTimer.Elapsed += LogWrite;
            _logTimer.Start();

        }
        public void WithDrowStop()
        {
            _logTimerWithdrow.Stop();
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
             return Parking.LastTransactionInfo.ToArray();
        }

        public string ReadFromLog()
        {
            return _logService.Read();
        }

        public void Dispose()
        {

        }
    }
}