﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System;
using System.IO;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimeServer:ITimerService
    { 
        
        private Timer aTimer = new System.Timers.Timer();
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public void FireElapsedEvent()
        {
            Elapsed?.Invoke(this, null);
        }

       
        public bool Enabled = false;

        public void Start()
        {
            aTimer.Elapsed += new ElapsedEventHandler(Elapsed);
            aTimer.Interval = this.Interval;
            aTimer.Enabled = true;

            
        }

        public void Stop()
        {
        }
       
        public void Dispose()
        {
            this.Elapsed = null;
            this.Stop();

        }

    }
}